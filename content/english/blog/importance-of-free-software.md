---
title: "My adventures with Dino illustrate the importance of Software Freedom"
date: 2022-11-20T12:57:34+05:30
author: Pirate Praveen
draft: false
discussionlink: https://codema.in/d/yiAdUeaW/a-blog-post-on-praveen-s-adventures-in-dino
---
I have [Librem 5 phone](https://puri.sm/products/librem-5/) and I use XMPP for communications. I was using [Dino](https://dino.im), but a few days ago, I learnt that Dino restricts key sharing to contacts. That means, in encrypted groups, I was not able to decrypt messages sent by people not in my contacts. Other Dino users also reported similar problems. The solution was to add them to my contacts, but that is impractical in a large encrypted group. So, I couldn't use Dino unless this policy was removed. The other option was Gajim, which did not have support for small screen devices, such as my Librem 5, and Gajim developers were not interested in UI that works on both mobiles and desktop.

I decided to fix the Dino key publishing issue myself. Going through the Dino's repository, I found an earlier commit which allowed key exchange with everyone, and not just with contacts. I just had to change one line to mimic that commit, but the way Dino works changed since then, and I could not build it successfully.

I met Abraham Raji at Minidebconf 22 Palakkad, who looked at the code and figured out the [way to fix it](https://github.com/dino/dino/pull/1309)-- it was still a single line fix. 

I didn't want to use an older version of Dino for this feature, as the new version introduced another important feature-- showing group history. I needed GTK 4 version for this feature, which was not in PureOS, the operating system I was running in my Librem 5. To fix this, I switched to mobian(which is debian for mobiles). This finally gave the desired set up, although I had to give up on disk encryption.

By the above example, I would like to emphasize the freedoms that free/swatantra software gives and its importance to everyone. Think of a proprietary app, like WhatsApp. If I wanted to run it as I wish, it would not have been possible. Only WhatsApp developers could have fixed that, and as you see in this case, the Dino developers weren't interested in fixing my issue. They just recommended me to add every person in the group as contact. Note that even if you don't know how to fix the above problem, you can run my modified version of Dino. Even though I could not fix it myself, I was able to take help from someone who knew programming. This means we can collectively crowdfund and pay someone to fix the problem even if we are not able to fix things ourselves.

---
title: "Use F-Droid App Store For Freedom and Privacy in Android phones"
date: 2022-01-18
draft: false
author: "false"
tags: ["f-droid", "privacy", "android"]
discussionlink: https://codema.in/d/KAzGtcZm/a-blog-post-on-f-droid
---
We have been emphasizing the [importance of Free Software](https://en.wikipedia.org/wiki/Free_software_movement) (software which gives users freedom to use, study, modify and share) for years now and the problems with [nonfree/proprietary software](https://gnu.org/malware). For users of Android operating system, there is a very easy way to download and use Free Software-- via the [F-Droid app store](https://f-droid.org). This will help you in replacing proprietary/nonfree apps with Free Software ones, an action which is important to take control of your own device.

F-Droid is an app store for Android which contains only Free/Swatantra Software. It is a community-driven project which ensures that the app you download matches the source code. F-Droid also verifies the apps for any malicious code-- for example, whether the app has some tracking or not. F-Droid also lists [Anti-features](https://f-droid.org/en/docs/Anti-Features) for each app in the F-Droid app store. It will also give updates for the recent versions of the apps.

### Get Started

To get started, grab the apk file for the F-Droid app store by clicking on 'Download F-Droid option' from [F-Droid's official website](https://f-droid.org/en/). Allow your browser to download apk files on prompt. After the apk file is finished downloading, please open it and the F-Droid should be installed and shown in the app drawer. Open F-Droid and swipe down to update the repositories. Now you are ready to go!

A few app recommendations to get started with F-Droid:

- [Indic Keyboard](https://f-droid.org/en/packages/org.smc.inputmethod.indic/): Indic keyboard is a privacy-respecting keyboard which supports English and many Indian languages. It currently supports [23 languages](https://indic.app/#supported-languages) and 60 layouts.

After downloading Indic Keyboard, don't forget to set it as default keyboard.

- [Newpipe](https://f-droid.org/en/packages/org.schabi.newpipe/): Newpipe is a frontend of YouTube. With Newpipe, you can watch YouTube videos without giving away your privacy. Further, Newpipe does not show you any ads, lets you download the video/audio in your device, and you can stream videos in the background as well. 

- [Quicksy](https://www.f-droid.org/en/packages/im.quicksy.client/): A chatting app which respects your freedom, privacy, and at the same time provides convenience.

- [pep app](https://f-droid.org/en/packages/security.pEp/): App for reading and writing emails which automatically encrypts emails between two pep users. Check out our [email encryption guide](/blog/pep-guide).

### Support projects with donations

In the end, we would like to encourage you to donate to the projects behind the apps you download and enjoy! A lot of Free Software apps and services rely on donations by their users. And finally, don't forget to [donate to the F-Droid project](https://f-droid.org/en/donate). 

### Further Reading

- [FSF Europe's article on F-Droid](https://fsfe.org/activities/android/android.en.html).

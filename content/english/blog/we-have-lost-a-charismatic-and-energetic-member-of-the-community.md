---
title: "We Have Lost a Charismatic and Energetic Member of the Community"
date: 2023-09-28T00:14:23+05:30
author: FSCI
tags: ["RIP", "debconf"]
draft: false
---

On 13th September 2023, [Abraham Raji](https://abrahamraji.in/about/) from Kerala, an active member of FSCI, died in a water accident during a trip in the [DebConf 2023](https://debconf23.debconf.org/news/2023-09-14-mourning-abraham/).

Abraham was a passionate Free Software evangelist and an active member of various Free Software related communities. The revamped website theme of FSCI is his labour of love.

He was also a Debian Developer and led the design team of DebConf23 and helped in organizing several other local events in recent years. The DebConf23 website, t-shirt, badge were all designed by him.

Following projects paid their respects to Abraham Raji by dedicating their latest releases to him:

- [Tails (v5.17.1)](https://tails.net/news/version_5.17.1/index.en.html)
- [Hoppscotch (v2023.8.1)](https://github.com/hoppscotch/hoppscotch/releases/tag/2023.8.1)
- [WakaReadme (v0.2.5)](https://github.com/athul/waka-readme/releases/tag/v0.2.5)

Following are memories shared by people who knew him:

- [Farewell video by Vysakh Premkumar On 13th September 2023, .
  ](https://yewtu.be/watch?v=6n3_sabZy_w)
- [Blog post by Sahil Dhiman (member of FSCI)](https://blog.sahilister.in/2023/09/abraham-raji/)
- [Blog post by Ravi Dwivedi (member of FSCI)](https://ravidwivedi.in/posts/debconf23/#a-good-friend-lost)
- [Blog post by Jonathan Carter (Debian Project Leader)](https://jonathancarter.org/2023/09/21/debconf23)

> I had worked with Abraham when we were migrating FSCI's website to hugo. He was a very curious person & had attention to detail on how the new website should look. We used to ocassionally chat about nerdy stuff. The sudden news about his tragedy left me speechless & it took me some time to realize that he's no more. His passion towards FOSS was amazing, We lost a potential individual who could positively impact FOSS in India.

> -- karthik

<br>

> I met Abraham during FOSSMeet 2023 in NIT Calicut. He was a very calm and curious person and was also a great presenter!

> -- aryak

<br>

> I met Abraham in mini debconf palakkad.Then after one month In villupuran(district in Tamil nadu) planned to conduct mini debconf in tamil nadu that time weekly once we have connected in jitsi meet regarding mini debconf plan. He was such a kind and responsible person. he alway gave respect to all person words. Jan,2023 he came to villupuram for mini debconf That time we discussed more about the foss and how we can spread foss in each and very corner of the society.

> -- viji, VGLUG Foundation

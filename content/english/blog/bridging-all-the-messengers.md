---
title: "How To Create A Channel Interconnected With Telegram, IRC, XMPP, Matrix"
date: 2022-03-27T02:14:41+05:30
author: "false"
tags: ["bridging", "interoperability"]
draft: false
discussionlink: https://codema.in/d/i8zpQHqS/write-a-blog-post-on-bridging-all-the-rooms-matrix-xmpp-telegram-irc
---
(Updated on 08-April-2022 to add screenshots)

Let's say your friend uses Telegram and wants to join your group on Free Software activism, while your group is on Matrix and the friend needs to download an extra app to join your group. Wouldn't it be better if users could connect with each other irrespective of the app they use? While we don't know any such method for encrypted personal chats or private groups as of now, but public groups can be interconnected in such a way that users of Telegram, IRC, Matrix, XMPP, can participate without switching to any other chat protocol. This tutorial is exactly about this.

This method will enable, for example, a Matrix user's post in the Matrix group to reach IRC, XMPP and Telegram users too. A posting by a user of any of these chatting systems in the respective group will be bridged across to other chat systems. 

Step 1: Create a public non-encrypted public Matrix room on any matrix homeserver.

Step 2: Create a public Telegram channel.

Step 3: Use [t2bot guide](https://t2bot.io/telegram/) to bridge Matrix with Telegram.

Step 4: In this step, we will bridge Matrix room to IRC.

4a: Create an IRC channel on oftc.net (or your preferred server) with the channel name, let's say, `#name`. To do this, we join irc.oftc.net via an IRC client and then in the chat box, type the command `/join #name`. You joined a new channel named `#name`. Type the command `/msg ChanServ HELP` in the chat box. Now go to the chat box of ChanServ, and type the command `REGISTER #name <Description of the channel>`. This will register a channel on irc.oftc.net.

4b: Make sure that you have admin access on the Matrix room and in the Matrix room, select the option 'Add widgets, bridges & bots' as in the image below(we are using Element Desktop app for Matrix side screenshots in this tutorial):

<img src="/img/bridge1.png" width="200">

4c: Select the option 'Add integrations' as shown in the image below.
 
<img src="/img/bridge2.png" height="200">

4d: Scroll down to the bridges section and choose the IRC option.

<img src="/img/bridge3.png" height="200">

4e: Choose the IRC network your channel is on. 

<img src="/img/bridge4.png" height="200">

4f: Fill all the channel details and in the nick, write the name of the channel operator. After filling details, click on 'Request Integration'.

<img src="/img/bridge5.png" height="200">

4g: Check the IRC side. You must have received a request from the Matrix bridge as shown in the image below, which you need to reply by entering either `y` or `yes` in the chatbox and pressing enter.

<img src="/img/bridge6.png" height="200">

4i: After you type y, the IRC channel and Matrix room will be bridged. This means IRC users can post in Matrix room and vice versa.

<img src="/img/bridge7.png" height="200">

Step 5: Use [IRC Cheogram](https://irc.cheogram.com/) to join the channel from the XMPP side.

If you are having problems joining from the XMPP side, please go to the room settings and change your nickname in your XMPP app. Changing nickname in xmpp group settings will change nickname on IRC.

<b>Note: For FSCI chat room, we have made Telegram channel as read-only to encourage participation through IRC, Matrix or XMPP. Telegram is centralized and we would not like to promote it. You are invited to <a href="/blog/bridging-all-the-messengers/#join-us">join our chat groups</a>.

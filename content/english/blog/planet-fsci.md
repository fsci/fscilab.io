---
title: "Planet FSCI"
date: 2025-02-10T23:55:20+05:30
author: Sahil Dhiman
author_about: "https://sahilister.in/"
tags: ["fsci", "planet", "blogs"]
draft: false
---
In the age of "AI in everything", one could wonder if writing and maintaining an individual blog is still relevant.  "AI in everything" [LLMs](https://en.wikipedia.org/wiki/Large_language_model) has gobbled up millions of lines of texts, audio, code and what not to generate its responses. Can an individual do better through their blog? Uncertain. But again, individual blogs are for a person's own expression; of their thoughts, understandings and world view. It's the human (touch).

Now coming to the topic, blog planet and [Planet FSCI](https://planet.fsci.in/). Blog planet(s) serve as blog aggregators where writings from community members land up. Whenever someone posts on their individual blog, planet software automatically fetches it make it part of Planet feed. It's a look at what's happening in the community and what new adventures are people up to. It feels like blog planets are historic "social media timelines" where you go to check in on your friends and their whereabouts. Blog planets seem to exist as long a Free Software communities exists. Quite a lot of big projects seems to have them like [Debian](https://planet.debian.org/), [DGPLUG](https://planet.dgplug.org/), [GNOME](https://planet.gnome.org/) and [others](https://gitlab.com/fsci/blog.fsci.org.in#other-planets-in-alphabetical-order).

_Side note - I'm curious to know if blog planets existed outside free software communities. Let me know if you know any instances of these._

On a similar theme, FSCI has a planet too for community members. Planet FSCI can be read via multiple avenues. I personally prefer going through the website itself, i.e. https://planet.fsci.in/. I do have [Atom Feed](https://planet.fsci.in/atom.xml) subscribed through my feed reader (Thunderbird on laptop and Feeder on Mobile.). New posts also get automatically posted in #FSCI (Matrix/XMPP/IRC) main channel.

Community members and Free Software enthusiasts are encouraged to join. We never really fully discussed and documented who can join Planet FSCI, but most will agree if you're into Free Software and technology in general, you're fit to join. These are not hard restrictions though for your blog posts too. Just [be nice](https://thejeshgn.com/2024/04/22/be-nice/). You can send a merge request [here](https://gitlab.com/fsci/blog.fsci.org.in#steps-to-add-blog) with RSS or Atom feed of your blog. Sending a mail with feed link to admin AT fsci.in works too. I'll appreciate, if the feed has text in full and not just summary. Planets are not link aggregators and I (like many others) love to read your blog in my feed reader only (rather than switching to my browser just to read your blog), thanks!

If you don't have a blog, may this serve as the motivation to start one. [DGPLUG](https://www.dgplug.org/)'s article on [blogging](https://summertraining.readthedocs.io/en/latest/blogging.html) comes as a much recommended read. I too, wrote about [my motivation to write](https://blog.sahilister.in/2020/10/why-i-write-blogs/), back in 2020. The blog takes the personality of its author. Technical writings, creative outflows, little tips and tricks or simple musings in life, personal blogs carry everything. My blog is one of my outflows. I have had interesting discussions on topics I deeply care about simply because people have read my blogs and met me to discuss different perspective and share information with me. That's a bliss of writing. Another way, I look as this blog writing as writing my own history. It's been almost 5 years, since I started writing online (and still sometimes wonder that's long time to continue with a hobby). I still keep going back to some of my writings just to understand my mindset then, and in general they're fun to read. 

Blogs are windows to someone's thoughts and life, and Planet is the town square where you can peer  ;)
 
In conclusion, this writing on FSCI Planet (or Planet FSCI) is going to come up on Planet FSCI as well. See you there. ;)
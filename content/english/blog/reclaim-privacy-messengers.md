---
title: Reclaim privacy in instant messaging with Free Software and choice of service providers
date: 2021-02-17T13:27:13+05:30
author: "false"
tags: ["privacy", "freedom", "whatsapp", "instant messaging", "federation"]
discussionlink: https://codema.in/d/CTeAEuGh/reclaim-privacy-in-instant-messaging-with-free-software-and-choice-of-service-providers
draft: false
---
Free Software Foundation of India [released a comparison of different messaging apps and services](https://fsf.org.in/article/better-than-whatsapp/) a few weeks ago. When compared to centralized services like WhatsApp and Telegram, services like Matrix and XMPP which respects users' freedom (as a user or as a community) and does not lead to [vendor lock-in](https://en.wikipedia.org/wiki/Vendor_lock-in) are better according to FSF India. This is because Matrix and XMPP are open protocols that allow federation and have free software implementations to run on servers and clients.

 <div style="display:flex">
 <img src="https://fsf.org.in/assets/img/ims.png" alt="Comparison on Instant Messengers - Infographics by Riya Sawant for FSF India" class="small-img" style="margin:auto">
 </div>

## Why is free software essential for your freedom?
[Free software](https://www.gnu.org/philosophy/free-sw) means software that respects users' freedom and community. Roughly, it means that the users have the freedom to run, copy, distribute, study, change and improve the software. Free Software gives users a collective control over the software. If the software is nonfree, then the users do not control the software. For your freedom's sake, you should avoid running any nonfree software.

As noted in the FSF India's article, free software is not enough to take full control of your communications. If the software is free, but the servers are proprietary and centralized, they can impose any conditions on your use of their services, and you would be forced to accept those conditions to continue using the service. Therefore, we need something more in addition to free software to take full control of our messaging systems, and so we introduce the concept of federated messaging systems.

## Federated messaging systems
Federation is a collection of independent service providers whose participants can communicate with each other. An example of federation is phone network. You can use any service provider's SIM and any mobile phone model of your choice and contact to any other user participating in the mobile network irrespective of their choice of SIM and mobile phone model.

Examples of federated messaging systems are [Matrix](https://matrix.org/) and [XMPP](https://xmpp.org/). You can use any app which can communicate using Matrix and any Matrix service provider and communicate with other Matrix users irrespective of their choice of app and service provider. For example, user Alice may be using a Matrix app, say A, and have an account on [poddery.com](https://poddery.com/) (FSCI's matrix server) and another user Bobby may be using another Matrix app, say B, and have an account on another similar service (say, disroot.org). Alice and Bobby can talk to each other even though they signed up on two different servers and are using different apps(but the apps must be able to communicate using Matrix protocol).

## Why do federated communication systems give users full control over their communications?
Since users have the freedom to run their own service in federated systems, they can set their own policies. Users can also choose a trusted service provider according to their policies. For example, if a user does not want to reveal their phone number or email, they can choose a service provider which allows them to sign up without requiring those details (say, phone number and email). Running your own server also gives you control over the [metadata retention](https://www.eff.org/deeplinks/2013/06/why-metadata-matters). The personal messages on Matrix are end-to-end encrypted by default. For XMPP, remember to choose an app which supports [OMEMO encryption](https://omemo.top/).

Matrix allows [exchanging data and messages with other platforms](https://matrix.org/bridges/) [using an Open Standard](https://matrix.org/docs/spec/). This is known as bridging. For example, Matrix and XMPP users can talk to each other using the [Matrix-XMPP bridge](https://github.com/matrix-org/matrix-bifrost) and Matrix users can join XMPP groups and vice versa using this bridge. This gives users freedom to communicate with users using apps and services that run on a different standard. Please note that XMPP-Matrix bridge does not allow end-to-end encryption as of now. Developments are going on which [will allow encrypting messages while bridging](https://messaginglayersecurity.rocks/mls-architecture/draft-ietf-mls-architecture.html) in the future.

With Matrix, users have the freedom to connect to the services in multiple ways and the creative freedom to build bots and other integrations that enhance the utility of discussions. For example, in [FSCI's matrix discussion room](https://chat.poddery.com/#/room/#fsci:poddery.com) we use a bot named "RSS Bot" which updates the room with the latest posts from various blogs related to free software. People can connect to these services from any software that is capable of communicating the open protocols that power these services. That enables our users to choose from a wide variety of [clients](https://matrix.org/clients/) to their liking and needs. This way federation gives users full control over their communications.

## Community run messaging services
Free Software Community of India runs its own Matrix and XMPP servers. The services that FSCI run are decentralized and federated. The users who connect to FSCI's services are able to communicate with not just other users signed up to FSCI's services, but also to anyone in the world who is using the same protocol such as Matrix or XMPP.

This community owned messaging infrastructure allows the users to meet all their common messaging needs. In doing so, neither do they have to run non-free software on their devices (and give away their freedom) nor are they forced to use any specific application or server. 

To run these services, FSCI relies on donations and crowdfunding campaigns. Therefore, we do not have to rely on advertisements or selling user data to sustain our services. In fact, we recognize such practices as injustices and this is one of the reasons we run our own service. This in turn makes community-run infrastructure like ours attractive to many citizens who are concerned about the onslaught by big-tech on their privacy and freedom. Running these services gives us full control over our own communications.

We invite you to sign up for an account on [poddery.com](https://poddery.com/) and take back control of your communications. If you have any queries in this regard, we would be glad to help you. You can contact us by [clicking here](https://fsci.in/#join-us).

